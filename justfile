install:
    poetry install --no-root

black:
    poetry run black . --check --diff

black-fix:
    poetry run black .

isort:
    poetry run isort . --check --diff

isort-fix:
    poetry run isort .

lint: black isort

lint-fix: black-fix isort-fix

new-problem difficulty problem:
    poetry run python utils/problem_folder_creator.py '{{difficulty}}' '{{problem}}'