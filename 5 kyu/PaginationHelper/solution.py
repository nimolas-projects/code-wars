# TODO: complete this class


class PaginationHelper:

    # The constructor takes in an array of items and a integer indicating
    # how many items fit within a single page
    def __init__(self, collection, items_per_page):
        self.pages = {}

        page_index = 0
        for index in range(0, len(collection), items_per_page):
            self.pages.update(
                {page_index: len(collection[index : index + items_per_page])}
            )
            page_index += 1

    # returns the number of items within the entire collection
    def item_count(self):
        return sum(self.pages.values())

    # returns the number of pages
    def page_count(self):
        return list(self.pages)[-1] + 1

    # returns the number of items on the current page. page_index is zero based
    # this method should return -1 for page_index values that are out of range
    def page_item_count(self, page_index):
        try:
            return self.pages[page_index]
        except KeyError:
            return -1

    # determines what page an item is on. Zero based indexes.
    # this method should return -1 for item_index values that are out of range
    def page_index(self, item_index):
        if item_index > sum(self.pages.values()) - 1 or item_index < 0:
            return -1

        cumulative = 0
        for key, value in self.pages.items():
            if item_index >= cumulative and item_index <= cumulative + value:
                return key
            else:
                cumulative += value
