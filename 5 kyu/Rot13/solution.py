lower_alphabet = [chr(ord_value) for ord_value in range(ord("a"), ord("a") + 26)]
upper_alphabet = [chr(ord_value) for ord_value in range(ord("A"), ord("A") + 26)]


def rot13(message: str):
    result = ""

    for letter in message:
        is_lower = letter.islower()

        if not letter.isalpha():
            result += letter
            continue

        result += (
            lower_alphabet[(lower_alphabet.index(letter) + 13) % len(lower_alphabet)]
            if is_lower
            else upper_alphabet[
                (upper_alphabet.index(letter) + 13) % len(upper_alphabet)
            ]
        )

    return result
