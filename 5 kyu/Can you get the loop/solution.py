def loop_size(node):
    found_nodes = {}
    nodes = 0

    found_nodes.update({node: nodes})

    next_node = node.next
    nodes += 1
    while next_node not in found_nodes:

        found_nodes.update({next_node: nodes})

        next_node = next_node.next
        nodes += 1

    return nodes - found_nodes[next_node]
