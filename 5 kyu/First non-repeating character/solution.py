def character_counts(string: str):
    lower_case = string.lower()
    unique_characters = {}

    for char in string:
        lower_char = char.lower()

        unique_characters[lower_char] = {
            "count": lower_case.count(lower_char),
            "case": "lower" if char.islower() else "upper",
        }

    return unique_characters


def first_non_repeating_letter(string: str):
    for key, value in character_counts(string).items():
        if value["count"] == 1:
            return key.lower() if value["case"] == "lower" else key.upper()

    return ""
