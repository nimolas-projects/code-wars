def generate_hashtag(s: str):
    new_word = "".join(word.strip().title() for word in s.split())

    if len(s) == 0 or len(new_word) > 140:
        return False

    return "#" + new_word
