def rgb(r, g, b):
    # your code here :)
    return f"{int_to_hex(r)}{int_to_hex(g)}{int_to_hex(b)}"


def int_to_hex(number):
    if number > 255:
        return "FF"
    if number < 0:
        return "00"

    return f"{number:0{2}x}".upper()
