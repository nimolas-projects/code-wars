import codewars_test as test
from solution import *


@test.describe("Sample tests")
def sample_tests():
    @test.it("Tests")
    def it_1():
        test.assert_equals(to_underscore("TestController"), "test_controller")
        test.assert_equals(to_underscore("MoviesAndBooks"), "movies_and_books")
        test.assert_equals(to_underscore("App7Test"), "app7_test")
        test.assert_equals(to_underscore(1), "1")
