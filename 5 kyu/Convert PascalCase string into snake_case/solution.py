def to_underscore(string):
    result = ""

    if type(string) != str:
        string = str(string)

    for index, character in enumerate(string):
        if index == 0:
            result += character.lower()
            continue

        if character.upper() == character and not character.isdigit():
            result += "_"

        result += character.lower()

    return result
