def move_zeros(lst: list[int]):
    non_zeros = [num for num in lst if num != 0]

    return non_zeros + [0] * lst.count(0)
