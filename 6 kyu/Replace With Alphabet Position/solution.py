def alphabet_position(text: str):
    alphabet = "abcdefghijklmnopqrstuvwxyz"
    return " ".join(
        str(alphabet.index(letter.lower()) + 1) for letter in text if letter.isalpha()
    )
