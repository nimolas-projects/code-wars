def create_phone_number(n):
    if not all([num in range(0, 10) for num in n]):
        return ""

    bracket = "".join(str(l) for l in n[0:3])
    before_dash = "".join(str(l) for l in n[3:6])
    after_dash = "".join(str(l) for l in n[6:])

    return f"({bracket}) {before_dash}-{after_dash}"
