def solution(s: str):
    result = s

    if len(s) % 2 != 0:
        result += "_"

    return [a + b for a, b in zip(result[::2], result[1::2])]
