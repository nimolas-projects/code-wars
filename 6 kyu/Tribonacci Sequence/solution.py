def tribonacci(signature: list, n):
    if n == 0:
        return []

    if n < 4:
        return signature[:n]

    result = signature

    for _ in range(n - len(signature)):
        result.append(sum(result[-3:]))

    return result
