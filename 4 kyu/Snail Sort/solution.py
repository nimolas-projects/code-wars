def snail(snail_map):
    visited_map = [
        [{"value": value, "visited": False} for index, value in enumerate(row)]
        for row in snail_map
    ]

    visited_values = []
    directions = [False] * 4  # 0 = right, 1 = down, 2 = left, 3 = up
    current_position = 0, 0

    current_value = visited_map[current_position[0]][current_position[1]]["value"]
    visited_map[current_position[0]][current_position[1]]["visited"] = True

    visited_values.append(current_value)
    while not all(directions):
        next_direction = directions.index(False)

        cant_move = False
        while not cant_move:
            print("cant_move", cant_move)
            print("next_direction", next_direction)
            if next_direction == 0:
                try:
                    if (
                        visited_map[current_position[0]][current_position[1] + 1][
                            "visited"
                        ]
                        == True
                    ):
                        cant_move = True
                        directions[next_direction] = True
                    else:
                        current_position = current_position[0], current_position[1] + 1
                except IndexError:
                    cant_move = True
                    directions[next_direction] = True

    return visited_values


print(snail([[1, 2, 3], [8, 9, 4], [7, 6, 5]]))
