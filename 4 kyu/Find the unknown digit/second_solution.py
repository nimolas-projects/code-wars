def solve_runes(runes):
    known_numbers = [int(number) for number in runes if number.isdigit()]

    numbers = sorted(list(set(range(10)) - set(known_numbers)))

    for number in numbers:
        replaced_runes = replace_question_marks(runes, number)
        result = find_result(replaced_runes)
        expression = find_expression(replaced_runes)

        if (len(result) > 1 and result[0] == "0") or result[0:2] == "-0":
            continue

        if eval(expression) == int(result):
            return number

    return -1


def replace_question_marks(string: str, number: int):
    return string.replace("?", str(number))


def find_result(string):
    return string[string.find("=") + 1 :]


def find_expression(string):
    return string[: string.find("=")]
