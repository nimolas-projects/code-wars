from second_solution import solve_runes
import codewars_test as test


@test.describe("Find the unknown digit")
def desc1():
    @test.it("Sample tests")
    def it1():
        test.assert_equals(solve_runes("1+1=?"), 2, "Answer for expression '1+1=?' ")
        test.assert_equals(
            solve_runes("123*45?=5?088"), 6, "Answer for expression '123*45?=5?088' "
        )
        test.assert_equals(
            solve_runes("-5?*-1=5?"), 0, "Answer for expression '-5?*-1=5?' "
        )
        test.assert_equals(
            solve_runes("19--45=5?"), -1, "Answer for expression '19--45=5?' "
        )
        test.assert_equals(
            solve_runes("??*??=302?"), 5, "Answer for expression '??*??=302?' "
        )
        test.assert_equals(
            solve_runes("?*11=??"), 2, "Answer for expression '?*11=??' "
        )
        test.assert_equals(
            solve_runes("??*1=??"), 2, "Answer for expression '??*1=??' "
        )
        test.assert_equals(
            solve_runes("-7*-8894?=6??594"),
            2,
            "Answer for expression '-7*-8894?=6??594' ",
        )
        test.assert_equals(
            solve_runes("3326-?6419=-?3093"),
            5,
            "Answer for expression '3326-?6419=-?3093' ",
        )
        test.assert_equals(
            solve_runes("-2972-?1322=-?4294"),
            5,
            "Answer for expression '-2972-?1322=-?4294' ",
        )
        test.assert_equals(
            solve_runes("-1412+?3329=?1917"),
            5,
            "Answer for expression '-1412+?3329=?1917' ",
        )
