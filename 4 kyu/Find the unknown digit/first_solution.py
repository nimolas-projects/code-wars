def solve_runes(runes):
    # Your code here

    known_numbers = [int(number) for number in runes if number.isdigit()]

    numbers = sorted(list(set(range(10)) - set(known_numbers)))

    for number in numbers:
        replaced_runes = replace_question_marks(runes, number)

        operand_1, operand_2 = find_operands(replaced_runes)
        operator = find_operator(replaced_runes)
        result = find_result(replaced_runes)

        if (len(result) > 1 and result[0] == "0") or result[0:2] == "-0":
            continue

        if do_math(int(operand_1), int(operand_2), operator) == int(result):
            return number

    return -1


def find_result(string):
    return string[string.find("=") + 1 :]


def find_operator(string: str):
    for index, character in enumerate(string):
        if character in ["+", "-", "*"] and index != 0:
            return character

    raise ValueError("we fucked up")


def find_operands(string: str):
    for index, _ in enumerate(string):
        if string[index + 1] in ["+", "-", "*"]:
            operand_1 = string[: index + 1]
            operator_index = index + 1
            break

    operand_2 = string[operator_index + 1 : string.find("=")]

    return operand_1, operand_2


def do_math(operand_1: int, operand_2: int, operator: str):
    math_function = {
        "+": lambda a, b: a + b,
        "-": lambda a, b: a - b,
        "*": lambda a, b: a * b,
    }

    return math_function.get(operator)(operand_1, operand_2)


def replace_question_marks(string: str, number: int):
    return string.replace("?", str(number))
