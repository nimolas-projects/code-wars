from pathlib import Path
from sys import argv


def _create_solution(path: Path):
    open(path / "solution.py", "x")


def _create_tests(path: Path):
    open(path / "tests.py", "x")


def _create_readme(path: Path):
    open(path / "README.md", "x")


def create_directory(difficulty: str, problem_name: str):
    difficulty_dir = Path(difficulty).absolute()
    problem_dir = Path(f"{difficulty}/{problem_name}").absolute()

    if not difficulty_dir.exists():
        difficulty_dir.mkdir()

    if not problem_dir.exists():
        problem_dir.mkdir()

    _create_solution(problem_dir)
    _create_tests(problem_dir)
    _create_readme(problem_dir)


if __name__ == "__main__":
    args = argv

    difficulty, problem = args[1:]

    create_directory(difficulty, problem)
